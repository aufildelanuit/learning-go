package main

import (
   "fmt"
   "os"
   "strconv"
   "math"
)

func power(a, b float64) (float64) {
   return math.Pow(a, b)
}

func main() {
   /* get command line arguments with:
   argsWithoutProg := os.Args[1:]
   fmt.Println(argsWithoutProg) */

   // get arguments from the command-line (excluding the executable name)
   args := os.Args[1:]
   // check that we have a sufficient amount of arguments
   if len(args) >= 2 {
      // try to parse float values from (string) arguments
      f1, err1 := strconv.ParseFloat(args[0], 64)
      f2, err2 := strconv.ParseFloat(args[1], 64)
      // if we couldn't parse any numbers from the arguments, return an error
      if ((err1 != nil) || (err2 != nil)) {
         // note: built-in println should output to stderr
         println("Error: Please provide two numerical arguments for this program to work.")
      } else {
         // while fmt.Println should write to stdout
         // Go on with the calculation if everything was ok thus far
         fmt.Println(power(f1, f2))
         // additional warning in case more than 2 arguments were given
         if len(args) > 2 {
            println("Note: only the first two parameters are taken into account here.")
         }
      }
   } else if len(args) < 2 {
      // in case we didn't get enough arguments from the command-line
      println("Error: This program requires at least two (numerical) arguments.")
   }
}
