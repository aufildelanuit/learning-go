package main

import (
   "fmt"
   "os"
   "strconv"
   "math/big"
   //"math"
)

type Cache struct {
   values [2]*big.Int
}

// returns the nth element of the fibonacci sequence
func fib_cached(n uint64, cache *Cache) *big.Int {
	nth_value := big.NewInt(0)
	if n < uint64(2) {
		nth_value = new(big.Int).SetUint64(n)
	} else {
		nth_value.Add(cache.values[0], cache.values[1])
	}
	cache.values[0] = cache.values[1]
	cache.values[1] = nth_value
	return nth_value
}

func main() {
   // get arguments from the command-line (excluding the executable name)
   args := os.Args[1:]
   // check that we have a sufficient amount of arguments
   if len(args) >= 1 {
      // try to parse uint64 values from (string) arguments
      n, err := strconv.ParseUint(args[0], 10, 64)
      // if we couldn't parse any numbers from the arguments, return an error
      if (err != nil) {
         n = 0
      }
      // Go on with the calculation if everything was ok thus far
      cache := Cache{}
      for i := uint64(0); i < n; i++ {
	   	fib_i := fib_cached(i, &cache)
	   	//fmt.Println(fmt.Sprintf("%.0f", fib_i))
         fmt.Println(fib_i)
	  	}
   } else if len(args) < 1 {
      // in case we didn't get enough arguments from the command-line
      println("Error: This program requires at least one argument: number of elements to calculate (int).")
   }
}
